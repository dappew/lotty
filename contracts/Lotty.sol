pragma solidity ^0.4.23;

// Токен интерфэйсээр дамжуулж Эгэрэгийн шилжүүлэг хийнэ
contract Token {
    function transfer(address _to, uint _value) public returns (bool);
}

// Тулцан шудрага лотто
// 100 эгэрэг тутамд 1 сугалаа
// Долоо хоног бүр баасан гарагийн 18:00 цагт тохирол явгдана
// Ганцхан азтан 7 хоногийн турш цугласан нийт эгэрэгийн эзэн болно
// Жич: Тохирол бүрийн 10% нь хөгжүүлэгчдэд, 10% нь тохиролыг явуулсан хэн нэгэнд урамшуулал болон очино.
// Жичийн жич: Таслар авч хүрээгүй үлдсэн бутархай мөнгө бүр бидэнд хандивлагдана, баярлалаа :D
contract Lotty {
    struct TicketsPack {
        address owner;
        uint index;
        uint count;
    }

    address public owner;
    Token egereg; // Эгэрэг
    TicketsPack[] internal ticketsPacks;
    uint ticketsPacksCount = 0;

    // Хэд дэх 7 хоног дээр, хэн, хэдэн тасалбар эзэмшиж байна?
    mapping(uint => mapping(address => uint)) public ticketsCountOf;

    // Одоо хэд дэх 7 хоног дээр явж буйг илэрхийлнэ.
    uint public nthWeek;

    // Тохирол явагдах тов.
    // Solidity дээр цаг хугацааг epoch-оос (1970-01-01 00:00:00) хойших секундээр хэмджэг.
    uint public nextMatch;

    // 100 эгэрэг бүр нэг сугалаа. Эгэрэг бүр 100 нэгжид задардаг.
    // 100 * 100
    uint constant ticketPrice = 10000;

    // Тохирол бүр дээр нийт шагналын сангийн 10%-ийг,
    // мөн тасалбар авч хүрээгүй бутархай үлдэгдэл зэрэг нь хөгжүүлэгчдэд хандивлагдах болно :D
    uint public donation;

    constructor(address _tokenAddress) public {
        egereg = Token(_tokenAddress);
        owner = msg.sender;
        nthWeek = 1;

        // Тохирол баасан гарагийн 18:00 цагт явагддаг.
        // Доорх бүх тооцоонууд секундээр хэмжигдэнэ, `now` нь epoch-оос хойш тухайн блок хүртэлх секундыг илтгэнэ.
        // Epoch нь пүрэв гарагт таардаг тул 7 хоногийн тоог зэрэгцүүлж баасан гарагийн 18:00 цагтай тохируулахын тулд
        // `now`-аас 1 өдөр 18:00 цагийг хасч өгөх шаардлагатай.
        // Гэхдээ тохирлыг Улаанбаатарын цагаар буюу +08:00 цагийн бүст явуулах тул 1 өдөр 18:00 цагийг биш 1 өдөр 10:00 цагийг хасахад болно.
        // Үүнийгээ 7 хоногт хувааж бүхлийг нь авсанаар epoch-оос хойш хэдэн 7 хоног өнгөрсөн буюу хэдэн баасан гарагийн 18:00 цаг өнгөрсөнийг авна.
        // Гарсан бүхэл тоог эргүүлэн секундруу хувиргахын тулд 7 хоногт үржүүлж байна.
        // Ингээд дараагийн тохирлыг олохдоо 7 хоногийг нэмнэ.
        // Epoch пүрэв гарагаас эхэлдэг тул 1 өдөр, 18:00 цагаа эргүүлээд нэмэх шаардлагатай.
        // +08:00 цагийн бүсээ тооцсоноор 1 өдөр 10:00 цагийг нэмсэнээр дараагийн тохирол epoch-оос хойш хэд дэх секундэд болохыг авна.
        // Үүнээс цааш тохирол бүр дээр уг тоог 7 хоногоор нэмээд явахад болно.
        nextMatch = uint((now - 1 days - 10 hours) / 1 weeks) * 1 weeks + 1 weeks + 1 days + 10 hours;
    }

    // Манай хаягруу ether явуулж хандив өргөх боломжтой
    function() external payable {}

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    // ERC223 токен стандартын дагуу токеныг смарт контрактын хаягруу явуулж буй үед уг функц ажиллана.
    // Тасалбар авахын тулд Эгэрэгээ шууд уг смарт контрактын хаягруу явуулахад энэ функцээр хүлээн авах юм.
    // Уг функцээр мөнгө хүлээн авч тасалбар олгох, мөн тохирлыг явуулна.
    function tokenFallback(address _from, uint _amount, bytes) external returns(bool) {
        
        // Явуулж буй токен нь Эгэрэг байх шаардлагатай
        require(msg.sender == address(egereg));

        // 0-оос их эгэрэг байх шаардлагатай
        require(_amount > 0);

        // Эхлээд тухайн 7 хоног дууссан эсэхийг шалгана.
        // Хэрвээ дуусчихсан байвал энэ хүн маш азтай, учир нь:
        // 1. Шинэ 7 хоногийн хамгийн анхны хүн болно
        // 2. Өмнөх 7 хоногийн тохирлыг явуулна
        // 3. Ийм буянтай ажил хийсэнийхээ хариуд шагналын сангийн 10%-ийг хүртэнэ.
        if (now >= nextMatch) {
            
            // Мөн тухайн 7 хоногт хэн нэгэн тасалбар авсан байх ёстой
            if (ticketsPacksCount > 0) {
            
                // Тохирлын хамгийн чухал зүйл бол санамсаргүй тоо.
                // Өмнөх блокийн hash-аас өөр юу хамгийн санамсаргүй байх билээ.
                // Яагаад энэ блокийнх нь hash байж болдоггүй юм бэ гэж үү?
                // Учир нь хараахан олборлогдоогүй байгаа тул.
                // Тэгэхээр авсан hash-аа бүхэл тоо болгоод, нийт тасалбаруудын тоонд хуваагаад
                // үлдэгдлийг нь авсанаар азтай тасалбар санамсаргүйгээр сонгогдоно.
                uint random = uint(blockhash(block.number-1)) % (ticketsPacks[ticketsPacksCount-1].count) + 1;

                // Азтай тасалбарын эзэн хэн байв?
                address winner = ticketOwner(random);

                // Шагналын санд нийт хэдэн эгэрэг хуримтлагдсаныг харцгаая
                uint fund = ticketsPacks[ticketsPacksCount-1].count * ticketPrice;

                // Тийм ээ, танд баяр хүргье, та шагналаа гардаж авна уу.
                // Нийт шагналын сангийн 80%, үлдсэн нь яах юм бэ гэж үү?
                uint won = fund * 80/100;
                require(egereg.transfer(winner, won));
                emit Won(nthWeek, winner, won);

                // Уг тохирлыг явуулж буй хүнийг гомдоож болохгүй биздэ?
                // Түүнд 10%.
                uint selectorReward = fund * 10/100;
                require(egereg.transfer(_from, selectorReward));
                emit Won(nthWeek, _from, selectorReward);

                // Харин үлдсэн 10%-ийг хөгжүүлэгчдэд :D
                donation += fund * 10/100;
            }

            // Одоо шинэ 7 хоногоо эхлүүлэх цаг.
            nextMatch += 1 weeks;
            nthWeek += 1;
            ticketsPacksCount = 0;
        }

        // Явуулсан мөнгө нь хэдэн тасалбар авч чадахыг харцгаая.
        uint _ticketsCount = _amount/ticketPrice;

        // Тасалбар авч хүрэлгүй үлдсэн бутархай эгэрэг хөгжүүлэгчдэд :D
        donation += _amount % ticketPrice;

        if (_ticketsCount > 0) {
            ticketsCountOf[nthWeek][_from] += _ticketsCount;

            // Шинэ тасалбаруудыг хэн авсаныг бүртгэн авцгаая
            createTickets(_from, _ticketsCount);

            emit Booked(nthWeek, _from, _ticketsCount);
        }

        return true;
    }

    // Шинэ хөгжүүлэгчид сайн байна уу гэж хэлээрэй :D
    function transferOwnership(address newOwner) external onlyOwner {
        owner = newOwner;
    }

    // Хөгжүүлэгчид өлссөн бололтой :D
    function withdrawDonation() external onlyOwner {
        require(egereg.transfer(owner, donation));
        donation = 0;
    }

    function withdraw() external onlyOwner {
        owner.transfer(address(this).balance);
    }

    function ticketsCount() external view returns(uint) {
        return ticketsPacks[ticketsPacksCount-1].count;
    }

    //Хэн хэдэн тасалбар авсаныг бүртгэж авах
    function createTickets(address _taker, uint _ticketsCount) internal {
        uint lastIndex = ticketsPacksCount > 0 ? ticketsPacks[ticketsPacksCount-1].count : 0;
        TicketsPack memory _ticketsPack = TicketsPack(_taker, lastIndex + 1, lastIndex + _ticketsCount);

        if (ticketsPacks.length <= ticketsPacksCount) {
            ticketsPacks.push(_ticketsPack);
        } else {
            ticketsPacks[ticketsPacksCount] = _ticketsPack;
        }
    
        ticketsPacksCount++;
    }

    // Тэд дэх тасалбарын эзэн хэн бэ?
    function ticketOwner(uint _index) internal view returns (address) {
        int l = -1;
        uint r = ticketsPacksCount;
        while (uint(l + 1) < r) {
            uint mid = (uint(l) + r) / 2;
            if (ticketsPacks[mid].index <= _index && ticketsPacks[mid].count >= _index) {
                return ticketsPacks[mid].owner;
            } else if (ticketsPacks[mid].count < _index) {
                l = int(mid);
            } else {
                r = mid;
            }
        }
    }

    // Тохирлын азтан. Үндсэн азтан болон тохирлыг явуулсан хүмүүс үүгээр зарлагдах болно.
    event Won(uint indexed _nthWeek, address indexed _winner, uint _price);

    // Борлогдсон тасалбарууд үүгээр зарлагдана, хэд дэх тоглолт дээр, хэн, хэдэн тасалбар авав?
    event Booked(uint indexed _nthWeek, address indexed _taker, uint _amount);
}